package com.kodetesttask.adapters

import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.kodetesttask.R
import com.kodetesttask.activities.RecipeActivity
import com.kodetesttask.interfaces.RecipesApi
import com.kodetesttask.models.RecipeDetailBriefModel
import com.kodetesttask.util.KeysIntent.KEY_UUID
import com.kodetesttask.util.glideLoadImage
import com.kodetesttask.util.isValidContextForGlide
import kotlinx.android.synthetic.main.item_rv_recipes_similiar.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.jetbrains.anko.startActivity
import retrofit2.HttpException

class RecipesSimilarAdapter(
    private val ctx: Context,
    private val mRecipesSim: MutableList<RecipeDetailBriefModel>
) : RecyclerView.Adapter<RecipesSimilarAdapter.RecipesSimilarViewHolder>() {

    class RecipesSimilarViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val mCvItem: CardView = itemView.cvItem
        val mIvItemImage: ImageView = itemView.ivItemImage
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipesSimilarViewHolder {
        val layoutInflater = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_rv_recipes_similiar, parent, false)
        return RecipesSimilarViewHolder(layoutInflater)
    }

    override fun onBindViewHolder(holder: RecipesSimilarViewHolder, pos: Int) {
        val curr = mRecipesSim[pos]
        val recipesApi = RecipesApi.createRetrofit()
        val img = holder.mIvItemImage
        val bgAnim = img.background as AnimationDrawable
        bgAnim.start()
        CoroutineScope(Dispatchers.IO).launch {
            val request = recipesApi.getRecipeDetailsAsync(curr.uuid)
            withContext(Dispatchers.Main) {
                try {
                    val response = request.await()
                    if (response.isSuccessful) {
                        val images = response.body()!!.recipe.images
                        holder.mCvItem.setOnClickListener {
                            ctx.startActivity<RecipeActivity>(KEY_UUID to curr.uuid)
                        }
                        if (isValidContextForGlide(ctx)) {
                            glideLoadImage(ctx, images[0], img, bgAnim)
                        }
                    }
                } catch (e: HttpException) {
                    bgAnim.stop()
                    img.setBackgroundResource(R.drawable.placeholder_error)
                } catch (e: Throwable) {
                    bgAnim.stop()
                    img.setImageResource(R.drawable.placeholder_error)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return mRecipesSim.size
    }

    fun updateData(data: MutableList<RecipeDetailBriefModel>) {
        mRecipesSim.clear()
        mRecipesSim.addAll(data)
        notifyDataSetChanged()
    }
}