package com.kodetesttask.adapters

import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.kodetesttask.R
import com.kodetesttask.activities.RecipeActivity
import com.kodetesttask.models.RecipeElem
import com.kodetesttask.util.KeysIntent.KEY_UUID
import com.kodetesttask.util.glideLoadImage
import com.kodetesttask.util.isValidContextForGlide
import kotlinx.android.synthetic.main.item_rv_recipes.view.*
import org.jetbrains.anko.startActivity

class RecipesMainAdapter(
    private val ctx: Context,
    private val mRecipes: MutableList<RecipeElem>
) : RecyclerView.Adapter<RecipesMainAdapter.RecipesMainViewHolder>() {

    private val mRecipesCopy = emptyList<RecipeElem>().toMutableList()

    class RecipesMainViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val mCardView: CardView = itemView.cvItem
        val mIvItemImage: ImageView = itemView.ivItemImage
        val mTvItemName: TextView = itemView.tvItemName
        val mTvItemDescription: TextView = itemView.tvItemDescription
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipesMainViewHolder {
        val linearLayout = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_rv_recipes, parent, false)
        return RecipesMainViewHolder(linearLayout)
    }

    override fun onBindViewHolder(holder: RecipesMainViewHolder, pos: Int) {
        val curr = mRecipes[pos]
        val bgAnim = holder.mIvItemImage.background as AnimationDrawable
        bgAnim.start()
        if (isValidContextForGlide(ctx)) {
            glideLoadImage(ctx, curr.images[0], holder.mIvItemImage, bgAnim)
        } else {
            bgAnim.stop()
            holder.mIvItemImage.setBackgroundResource(R.drawable.placeholder_error)
        }
        holder.mTvItemName.text = curr.name
        holder.mTvItemDescription.text = curr.description
        holder.mCardView.setOnClickListener {
            ctx.startActivity<RecipeActivity>(KEY_UUID to curr.uuid)
        }
    }

    override fun getItemCount(): Int {
        return mRecipes.size
    }

    fun updateData(data: MutableList<RecipeElem>) {
        mRecipes.clear()
        mRecipes.addAll(data)
        mRecipesCopy.clear()
        mRecipesCopy.addAll(mRecipes)
        notifyDataSetChanged()
    }

    fun updateDataSearch(nText: String) {
        mRecipes.clear()
        if (nText.isEmpty()) {
            mRecipes.addAll(mRecipesCopy)
        } else {
            val text = nText.toLowerCase()
            mRecipesCopy.filterTo(mRecipes) {
                if (it.name.isEmpty()
                    && it.description.isEmpty()
                    && it.instructions.isEmpty()
                ) {
                    false
                } else {
                    it.name.toLowerCase().contains(text)
                            || it.description.toLowerCase().contains(text)
                            || it.instructions.toLowerCase().contains(text)
                }
            }
        }
        notifyDataSetChanged()
    }
}