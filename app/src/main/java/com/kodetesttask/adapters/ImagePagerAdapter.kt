package com.kodetesttask.adapters

import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.kodetesttask.R
import com.kodetesttask.activities.RecipeImagesActivity
import com.kodetesttask.util.KeysIntent.KEY_IMAGE_LINK
import com.kodetesttask.util.glideLoadImage
import com.kodetesttask.util.isValidContextForGlide
import org.jetbrains.anko.startActivity

class ImagePagerAdapter(private val ctx: Context, private val data: List<String>) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, pos: Int): Any {
        val layout =
            LayoutInflater.from(ctx).inflate(R.layout.item_image_pager_adapter, container, false)
        val img = layout.findViewById<ImageView>(R.id.ivImages)
        val bgAnim = img.background as AnimationDrawable
        bgAnim.start()
        if (isValidContextForGlide(ctx)) {
            glideLoadImage(ctx, data[pos], img, bgAnim)
        } else {
            bgAnim.stop()
            img.setBackgroundResource(R.drawable.placeholder_error)
        }
        layout.findViewById<ImageView>(R.id.ivImages).setOnLongClickListener {
            ctx.startActivity<RecipeImagesActivity>(KEY_IMAGE_LINK to data[pos])
            true
        }
        container.addView(layout)
        return layout
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view == obj
    }

    override fun destroyItem(container: ViewGroup, position: Int, view: Any) {
    }

    override fun getCount(): Int {
        return data.size
    }
}