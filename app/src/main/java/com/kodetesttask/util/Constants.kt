package com.kodetesttask.util

object Constants {
    const val IMAGES_NUM_ANIM_DURATION = 2500L
    const val KEY_RECIPE_DATA_TO_FRAGMENT = "KEY_RECIPE_DATA_TO_FRAGMENT"
}

object Urls {
    const val URL_API_RECIPES = "https://test.kode-t.ru/"
}

object Tags {
    const val TAG_RECIPE_FRAGMENT_CONTENT = "TAG_RECIPE_FRAGMENT_CONTENT"
    const val TAG_RECIPE_FRAGMENT_IMAGES = "TAG_RECIPE_FRAGMENT_IMAGES"
}

object KeysIntent {
    const val KEY_UUID = "KEY_UUID"
    const val KEY_IMAGE_LINK = "KEY_IMAGE_LINK"
}

object KeysMainSaveInstance {
    const val KEY_RECIPES_LIST = "KEY_RECIPES_LIST"
    const val KEY_RECIPES_LIST_NAME = "KEY_RECIPES_LIST_NAME"
    const val KEY_RECIPES_LIST_DATE = "KEY_RECIPES_LIST_DATE"
    const val KEY_SEARCH_QUERY = "KEY_SEARCH_QUERY"
    const val KEY_SORT_CHOICE = "KEY_SORT_CHOICE"
    const val KEY_ERROR_MESSAGE = "KEY_ERROR_MESSAGE"
}

object KeysRecipeSaveInstance {
    const val KEY_RECIPE_ELEM = "KEY_RECIPE_ELEM"
    const val KEY_CURR_IMAGE_POS = "KEY_CURR_IMAGE_POS"
}
