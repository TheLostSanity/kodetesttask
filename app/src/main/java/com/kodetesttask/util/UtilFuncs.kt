package com.kodetesttask.util

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.provider.MediaStore
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.kodetesttask.R

fun setTextOrGone(view: TextView, text: String?) {
    if (text.isNullOrBlank()) {
        view.visibility = View.GONE
    } else {
        view.visibility = View.VISIBLE
        view.text = text
    }
}

fun isValidContextForGlide(context: Context?): Boolean {
    if (context == null) {
        return false
    } else if (context is Activity && (context.isDestroyed || context.isFinishing)) {
        return false
    }
    return true
}

fun glideLoadImage(
    context: Context,
    url: String,
    imageView: ImageView,
    animation: AnimationDrawable = AnimationDrawable()
) {
    Glide
        .with(context)
        .load(url)
        .listener(object : RequestListener<Drawable> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>?,
                isFirstResource: Boolean
            ): Boolean {
                imageView.setBackgroundResource(R.drawable.placeholder_error)
                animation.stop()
                return false
            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                animation.stop()
                return false
            }
        })
        .fallback(R.drawable.placeholder_error)
        .centerCrop()
        .into(imageView)
}

fun saveImg(context: Context, image: Bitmap): Boolean {
    val values = ContentValues()
    var url: Uri? = null
    val cr = context.contentResolver
    with(values) {
        put(MediaStore.Images.Media.DATE_ADDED, System.currentTimeMillis() / 1000)
        put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis() / 1000)
        put(MediaStore.Images.Media.DATE_MODIFIED, System.currentTimeMillis() / 1000)
        put(MediaStore.Images.Media.MIME_TYPE, "image/png")
    }
    return try {
        url = cr.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        val imageOut = cr.openOutputStream(url)
        imageOut.use {
            image.compress(Bitmap.CompressFormat.PNG, 100, it)
        }
        true
    } catch (e: Exception) {
        if (url != null) {
            cr.delete(url, null, null)
        }
        false
    }
}