package com.kodetesttask.activities

import android.Manifest
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.kodetesttask.R
import com.kodetesttask.util.KeysIntent.KEY_IMAGE_LINK
import com.kodetesttask.util.saveImg
import com.master.permissionhelper.PermissionHelper
import kotlinx.android.synthetic.main.activity_recipe_images.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.toast
import org.jetbrains.anko.yesButton

class RecipeImagesActivity : AppCompatActivity() {

    private lateinit var data: String
    private lateinit var permissionHelper: PermissionHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe_images)
        setSupportActionBar(findViewById(R.id.toolbarRecipeImages))
        data = intent.getStringExtra(KEY_IMAGE_LINK)
        getImage()
        iv_photo.apply {
            isZoomable = true
            setOnClickListener {
                if (toolbarRecipeImages.visibility == View.VISIBLE) {
                    toolbarRecipeImages.visibility = View.INVISIBLE
                } else {
                    toolbarRecipeImages.visibility = View.VISIBLE
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_items_recipe_images, menu)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = ""
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.action_save -> {
                alert(message = getString(com.kodetesttask.R.string.alertDialogSave)) {
                    yesButton {
                        Glide
                            .with(this@RecipeImagesActivity)
                            .asBitmap()
                            .load(data)
                            .into(object : SimpleTarget<Bitmap>() {
                                override fun onResourceReady(reimage: Bitmap, transition: Transition<in Bitmap>?) {
                                    permissionHelper = PermissionHelper(
                                        this@RecipeImagesActivity,
                                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    )
                                    permissionHelper.request(object : PermissionHelper.PermissionCallback {
                                        override fun onPermissionGranted() {
                                            if (saveImg(this@RecipeImagesActivity, reimage)) {
                                                toast(getString(R.string.onSaveImage))
                                            } else {
                                                toast(getString(R.string.errorOnSaveImage))
                                            }
                                        }

                                        override fun onIndividualPermissionGranted(grantedPermission: Array<String>) {
                                        }

                                        override fun onPermissionDenied() {
                                            toast(getString(R.string.onPermStorageNot))
                                        }

                                        override fun onPermissionDeniedBySystem() {
                                        }
                                    })
                                }
                            })
                    }
                    noButton { }
                }.show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun getImage() {
        Glide.with(this)
            .load(data)
            .placeholder(R.drawable.animation_loading)
            .fallback(R.drawable.placeholder_error)
            .fitCenter()
            .into(iv_photo)
    }
}
