package com.kodetesttask.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.kodetesttask.R
import com.kodetesttask.adapters.RecipesMainAdapter
import com.kodetesttask.interfaces.RecipesApi
import com.kodetesttask.models.RecipeElem
import com.kodetesttask.util.KeysMainSaveInstance.KEY_ERROR_MESSAGE
import com.kodetesttask.util.KeysMainSaveInstance.KEY_RECIPES_LIST
import com.kodetesttask.util.KeysMainSaveInstance.KEY_RECIPES_LIST_DATE
import com.kodetesttask.util.KeysMainSaveInstance.KEY_RECIPES_LIST_NAME
import com.kodetesttask.util.KeysMainSaveInstance.KEY_SEARCH_QUERY
import com.kodetesttask.util.KeysMainSaveInstance.KEY_SORT_CHOICE
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.jetbrains.anko.longToast
import org.jetbrains.anko.selector
import org.jetbrains.anko.toast
import retrofit2.HttpException

class MainActivity : AppCompatActivity() {

    private val mRecipesAdapter = RecipesMainAdapter(this, emptyList<RecipeElem>().toMutableList())
    private val mRecipes = emptyList<RecipeElem>().toMutableList()
    private var mRecipesByDate = emptyList<RecipeElem>().toMutableList()
    private var mRecipesByName = emptyList<RecipeElem>().toMutableList()
    private var mSvQuery = ""
    private var mSortChoice = -1
    private lateinit var mSearchView: SearchView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbarMain))

        vPullToRefresh.setOnRefreshListener { getListOfRecipes() }
        rvRecipesMain.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = mRecipesAdapter
        }
        if (savedInstanceState != null) {
            getDataFromBundle(savedInstanceState)
            with(mRecipesAdapter) {
                if (mSortChoice == -1) {
                    updateData(mRecipes)
                } else {
                    setSortedDataToAdapter(mSortChoice)
                }
            }
        } else {
            vPullToRefresh.isRefreshing = true
            getListOfRecipes()
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.run {
            putParcelableArrayList(KEY_RECIPES_LIST, ArrayList(mRecipes))
            putParcelableArrayList(KEY_RECIPES_LIST_NAME, ArrayList(mRecipesByName))
            putParcelableArrayList(KEY_RECIPES_LIST_DATE, ArrayList(mRecipesByDate))
            if (tvErrNetworkMain.visibility == View.VISIBLE) {
                putString(KEY_ERROR_MESSAGE, tvErrNetworkMain.text.toString())
            }
            putString(KEY_SEARCH_QUERY, mSvQuery)
            putInt(KEY_SORT_CHOICE, mSortChoice)
        }
        super.onSaveInstanceState(outState)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_items_main, menu)
        supportActionBar!!.title = getString(R.string.toolbarTitleMain)
        val sv = menu!!.findItem(R.id.action_search)
        mSearchView = sv.actionView as SearchView
        mSearchView.maxWidth = Int.MAX_VALUE // Make SearchView expand properly
        if (mSvQuery.isNotBlank()) {
            sv.expandActionView()
            mSearchView.setQuery(mSvQuery, true)
            mRecipesAdapter.updateDataSearch(mSvQuery)

            // These 4 lines fix a case when sort button disappears
            menu.findItem(R.id.action_sort).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)
            mSearchView.setOnCloseListener {
                menu.findItem(R.id.action_sort).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM)
                true
            }
        }
        mSearchView.queryHint = getString(R.string.searchHint)
        mSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                mSearchView.clearFocus()
                mRecipesAdapter.updateDataSearch(query)
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                mRecipesAdapter.updateDataSearch(newText)
                mSvQuery = newText
                return true
            }
        })

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean = when (item?.itemId) {
        R.id.action_sort -> {
            selector(
                getString(R.string.sortDialogTitle),
                resources.getStringArray(R.array.sort_choices).toList()
            ) { _, which ->
                mSortChoice = which
                setSortedDataToAdapter(which)
            }
            true
        }
        R.id.action_refresh -> {
            vPullToRefresh.isRefreshing = true
            if (tvErrNetworkMain.visibility == View.VISIBLE) tvErrNetworkMain.visibility = View.INVISIBLE
            getListOfRecipes()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun getDataFromBundle(bundle: Bundle) {
        mRecipes.addAll(bundle.getParcelableArrayList<RecipeElem>(KEY_RECIPES_LIST)!!.toMutableList())
        mRecipesByName.addAll(bundle.getParcelableArrayList<RecipeElem>(KEY_RECIPES_LIST_NAME)!!.toMutableList())
        mRecipesByDate.addAll(bundle.getParcelableArrayList<RecipeElem>(KEY_RECIPES_LIST_DATE)!!.toMutableList())
        mSvQuery = bundle.getString(KEY_SEARCH_QUERY)!!
        mSortChoice = bundle.getInt(KEY_SORT_CHOICE)
        if (bundle.getString(KEY_ERROR_MESSAGE) != null) {
            tvErrNetworkMain.visibility = View.VISIBLE
            tvErrNetworkMain.text = bundle.getString(KEY_ERROR_MESSAGE)
        }
    }

    private fun setSortedDataToAdapter(dialogChoice: Int) {
        if (mRecipesByName.isEmpty()) {
            mRecipesByName = mRecipes.sortedWith(compareBy { it.name }).toMutableList()
        }
        if (mRecipesByDate.isEmpty()) {
            mRecipesByDate = mRecipes.sortedWith(compareBy { it.lastUpdated }).toMutableList()
        }
        when (dialogChoice) {
            0 -> mRecipesAdapter.updateData(mRecipesByName)
            1 -> mRecipesAdapter.updateData(mRecipesByName.asReversed())
            2 -> mRecipesAdapter.updateData(mRecipesByDate.asReversed())
            3 -> mRecipesAdapter.updateData(mRecipesByDate)
        }
        if (::mSearchView.isInitialized) {
            mSearchView.setQuery(mSearchView.query, true)
        }
        rvRecipesMain.smoothScrollToPosition(0)
    }

    private fun getListOfRecipes() {
        val recipesApi = RecipesApi.createRetrofit()
        CoroutineScope(Dispatchers.IO).launch {
            val request = recipesApi.getRecipesAsync()
            withContext(Dispatchers.Main) {
                try {
                    val response = request.await()
                    if (response.isSuccessful) {
                        val recipes = response.body()!!.recipes
                        if (mRecipes != recipes || recipes.isEmpty()) {
                            mRecipesByName.clear()
                            mRecipesByDate.clear()
                            mRecipes.addAll(recipes)
                            mRecipesAdapter.updateData(mRecipes)
                            tvErrNetworkMain.visibility = View.INVISIBLE
                            rvRecipesMain.visibility = View.VISIBLE
                        } else {
                            toast(getString(R.string.upToDate))
                        }
                    } else {
                        networkErrHandler(getString(R.string.errorServer, response.code().toString()))
                    }
                } catch (e: HttpException) {
                    networkErrHandler(e.message())
                } catch (e: Throwable) {
                    networkErrHandler(getString(R.string.errorConnection))
                }
                vPullToRefresh.isRefreshing = false
            }
        }
    }

    private fun networkErrHandler(error: String) {
        if (mRecipes.isNotEmpty()) {
            longToast(error)
        } else {
            tvErrNetworkMain.text = error
            tvErrNetworkMain.visibility = View.VISIBLE
            toast(error)
        }
    }
}
