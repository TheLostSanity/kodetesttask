package com.kodetesttask.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.kodetesttask.R
import com.kodetesttask.fragments.RecipeContentFragment
import com.kodetesttask.fragments.RecipeImagesFragment
import com.kodetesttask.interfaces.RecipesApi
import com.kodetesttask.models.RecipeDetailsElemModel
import com.kodetesttask.util.KeysIntent.KEY_UUID
import com.kodetesttask.util.KeysRecipeSaveInstance.KEY_RECIPE_ELEM
import com.kodetesttask.util.Tags.TAG_RECIPE_FRAGMENT_CONTENT
import com.kodetesttask.util.Tags.TAG_RECIPE_FRAGMENT_IMAGES
import com.kodetesttask.util.setTextOrGone
import kotlinx.android.synthetic.main.activity_recipe.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast
import retrofit2.HttpException

class RecipeActivity : AppCompatActivity() {

    private var mRecipeElem = RecipeDetailsElemModel()
    private var isRotated = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe)
        setSupportActionBar(findViewById(R.id.toolbarRecipe))

        if (savedInstanceState != null) {
            mRecipeElem = savedInstanceState.getParcelable(KEY_RECIPE_ELEM)!!
            isRotated = true
        }
        if (!isRotated) {
            progressBarRecipe.visibility = View.VISIBLE
            recipeLayout.visibility = View.INVISIBLE
        }
        getsetRecipeContent()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putParcelable(KEY_RECIPE_ELEM, mRecipeElem)
        super.onSaveInstanceState(outState)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_items_recipe, menu)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.toolbarTitleRecipe)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean = when (item?.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        R.id.action_refresh -> {
            if (tvErrNetworkRecipe.visibility == View.VISIBLE) tvErrNetworkRecipe.visibility = View.INVISIBLE
            getsetRecipeContent()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun getsetRecipeContent() {
        setTextOrGone(tvNameRecipe, mRecipeElem.name)
        val recipesApi = RecipesApi.createRetrofit()
        CoroutineScope(Dispatchers.IO).launch {
            val request = recipesApi.getRecipeDetailsAsync(intent.getStringExtra(KEY_UUID))
            withContext(Dispatchers.Main) {
                try {
                    progressBarRecipe.visibility = View.VISIBLE
                    val response = request.await()
                    if (response.isSuccessful) {
                        val recipeElem = response.body()!!.recipe
                        if (recipeElem.lastUpdated != mRecipeElem.lastUpdated) {
                            recipeLayout.visibility = View.INVISIBLE
                            mRecipeElem = recipeElem
                            setFragments()
                        } else {
                            if (!isRotated) {
                                toast(getString(R.string.upToDate))
                            }
                            setFragments()
                        }
                    } else {
                        networkErrHandler(getString(R.string.errorServer, response.code().toString()))
                    }
                } catch (e: HttpException) {
                    networkErrHandler(e.message())
                } catch (e: Throwable) {
                    networkErrHandler(getString(R.string.errorConnection))
                }
            }
        }
    }

    private fun networkErrHandler(error: String) {
        if (!mRecipeElem.isEmpty()) {
            longToast(error)
            progressBarRecipe.visibility = View.INVISIBLE
        } else {
            recipeLayout.visibility = View.INVISIBLE
            progressBarRecipe.visibility = View.INVISIBLE
            tvErrNetworkRecipe.text = error
            tvErrNetworkRecipe.visibility = View.VISIBLE
            toast(error)
        }
    }

    private fun setFragments() {
        setTextOrGone(tvNameRecipe, mRecipeElem.name)
        val mRecipeContentFragment = supportFragmentManager.findFragmentByTag(TAG_RECIPE_FRAGMENT_CONTENT)
        val mRecipeImagesFragment = supportFragmentManager.findFragmentByTag(TAG_RECIPE_FRAGMENT_IMAGES)
        if (mRecipeContentFragment == null) {
            supportFragmentManager.beginTransaction()
                .add(
                    R.id.layoutForFragmentRecipeContent,
                    RecipeContentFragment.newInstance(mRecipeElem),
                    TAG_RECIPE_FRAGMENT_CONTENT
                )
                .commitAllowingStateLoss()
        } else {
            (mRecipeContentFragment as RecipeContentFragment).setContent()
        }
        if (mRecipeElem.images.isEmpty()) {
            cvRecipeImages.visibility = View.GONE
        } else {
            if (mRecipeImagesFragment == null) {
                supportFragmentManager.beginTransaction()
                    .add(
                        R.id.cvRecipeImages,
                        RecipeImagesFragment.newInstance(mRecipeElem),
                        TAG_RECIPE_FRAGMENT_IMAGES
                    )
                    .commitAllowingStateLoss()
            } else {
                (mRecipeImagesFragment as RecipeImagesFragment).setContent()
            }
        }
        progressBarRecipe.visibility = View.INVISIBLE
        tvErrNetworkRecipe.visibility = View.INVISIBLE
        recipeLayout.visibility = View.VISIBLE
        isRotated = false
    }
}
