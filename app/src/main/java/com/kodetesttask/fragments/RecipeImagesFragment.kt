package com.kodetesttask.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kodetesttask.R
import com.kodetesttask.adapters.ImagePagerAdapter
import com.kodetesttask.models.RecipeDetailsElemModel
import com.kodetesttask.util.Constants.IMAGES_NUM_ANIM_DURATION
import com.kodetesttask.util.Constants.KEY_RECIPE_DATA_TO_FRAGMENT
import com.kodetesttask.util.KeysRecipeSaveInstance.KEY_CURR_IMAGE_POS
import kotlinx.android.synthetic.main.fragment_recipe_images.*

class RecipeImagesFragment : Fragment() {

    private var mImages: MutableList<String> = emptyList<String>().toMutableList()
    private var itemPos = 0
    private lateinit var mPager: ViewPager

    companion object {
        fun newInstance(data: RecipeDetailsElemModel): RecipeImagesFragment {
            val fragment = RecipeImagesFragment()
            val bundle = Bundle()
            bundle.putParcelable(KEY_RECIPE_DATA_TO_FRAGMENT, data)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_recipe_images, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        itemPos = savedInstanceState?.getInt(KEY_CURR_IMAGE_POS) ?: 0
        mPager = view.findViewById(R.id.imagesPager)
        val mRecipeElem = this.arguments?.getParcelable<RecipeDetailsElemModel>(KEY_RECIPE_DATA_TO_FRAGMENT)!!
        mImages.addAll(mRecipeElem.images)
        setContent()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(KEY_CURR_IMAGE_POS, mPager.currentItem)
        super.onSaveInstanceState(outState)
    }

    fun setContent() {
        mPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {
            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
            }

            override fun onPageSelected(pos: Int) {
                tvNumOfItems.text = getString(R.string.numOfImages, pos + 1, mImages.size)
                fadeAnimation()
            }
        })
        mPager.adapter = ImagePagerAdapter(context!!, mImages)
        tvNumOfItems.text = getString(R.string.numOfImages, 1, mImages.size)
        cvNumOfItems.animate().alpha(0f).duration = IMAGES_NUM_ANIM_DURATION + 1000L
        mPager.currentItem = itemPos
    }

    private fun fadeAnimation() {
        if (cvNumOfItems.animation != null) cvNumOfItems.animation.cancel()
        cvNumOfItems.alpha = 1f
        cvNumOfItems.animate().alpha(0f).duration = IMAGES_NUM_ANIM_DURATION
    }
}
