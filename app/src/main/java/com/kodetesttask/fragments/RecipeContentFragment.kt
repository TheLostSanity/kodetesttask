package com.kodetesttask.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kodetesttask.R
import com.kodetesttask.adapters.RecipesSimilarAdapter
import com.kodetesttask.models.RecipeDetailBriefModel
import com.kodetesttask.models.RecipeDetailsElemModel
import com.kodetesttask.util.Constants.KEY_RECIPE_DATA_TO_FRAGMENT
import com.kodetesttask.util.setTextOrGone
import kotlinx.android.synthetic.main.fragment_recipe_content.*

class RecipeContentFragment : Fragment() {

    private var mRecipeElem = RecipeDetailsElemModel()
    private lateinit var mRecipesSimAdapter: RecipesSimilarAdapter

    companion object {
        fun newInstance(data: RecipeDetailsElemModel?): RecipeContentFragment {
            val fragment = RecipeContentFragment()
            val bundle = Bundle()
            bundle.putParcelable(KEY_RECIPE_DATA_TO_FRAGMENT, data)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mRecipeElem = this.arguments?.getParcelable(KEY_RECIPE_DATA_TO_FRAGMENT)!!
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mRecipesSimAdapter = RecipesSimilarAdapter(context!!, emptyList<RecipeDetailBriefModel>().toMutableList())
        return inflater.inflate(R.layout.fragment_recipe_content, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bundle = this.arguments
        mRecipeElem = bundle?.getParcelable(KEY_RECIPE_DATA_TO_FRAGMENT)!!
        if (mRecipeElem.lastUpdated != -1) { // If received element is not empty
            setContent()
        }
    }

    @Suppress("DEPRECATION")
    fun setContent() {
        val stars = resources.obtainTypedArray(R.array.starsList)
        if (mRecipeElem.difficulty != -1) { // If there is difficulty
            ivDificulty.setImageResource(stars.getResourceId(mRecipeElem.difficulty - 1, 0))
        }
        stars.recycle()
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            setTextOrGone(
                tvRecipeDescription, getString(
                    R.string.recipeDescription,
                    Html.fromHtml(mRecipeElem.description, Html.TO_HTML_PARAGRAPH_LINES_CONSECUTIVE)
                )
            )
            setTextOrGone(
                tvRecipeInstruction, getString(
                    R.string.recipeInstruction,
                    Html.fromHtml(mRecipeElem.instructions, Html.TO_HTML_PARAGRAPH_LINES_CONSECUTIVE)
                )
            )
        } else {
            setTextOrGone(
                tvRecipeDescription,
                getString(R.string.recipeDescription, Html.fromHtml(mRecipeElem.description))
            )
            setTextOrGone(
                tvRecipeInstruction,
                getString(R.string.recipeInstruction, Html.fromHtml(mRecipeElem.instructions))
            )
        }
        mRecipesSimAdapter.updateData(mRecipeElem.similar)
        rvRecipesSim.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            adapter = mRecipesSimAdapter
        }
    }
}
