package com.kodetesttask.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class RecipeDetailsModel(
    val recipe: RecipeDetailsElemModel
)

@Parcelize
data class RecipeDetailsElemModel(
    val uuid: String = "",
    val name: String = "",
    val images: List<String> = emptyList(),
    val lastUpdated: Int = -1,
    val description: String = "",
    val instructions: String = "",
    val difficulty: Int = -1,
    val similar: MutableList<RecipeDetailBriefModel> = emptyList<RecipeDetailBriefModel>().toMutableList()
) : Parcelable {
    fun isEmpty(): Boolean = (uuid.isBlank()
            && name.isBlank()
            && images.isEmpty()
            && lastUpdated == -1
            && description.isBlank()
            && instructions.isBlank()
            && difficulty == -1
            && similar.isEmpty()
            )
}

@Parcelize
data class RecipeDetailBriefModel(
    val uuid: String = "",
    val name: String = ""
) : Parcelable