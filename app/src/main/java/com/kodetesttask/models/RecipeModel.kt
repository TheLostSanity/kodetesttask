package com.kodetesttask.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class RecipeModel(
    val recipes: MutableList<RecipeElem>
)

@Parcelize
data class RecipeElem(
    val uuid: String = "",
    val name: String = "",
    val images: List<String> = emptyList(),
    val lastUpdated: Int = -1,
    val description: String = "",
    val instructions: String = "",
    val difficulty: Int = -1
) : Parcelable