package com.kodetesttask.interfaces

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.kodetesttask.models.RecipeDetailsModel
import com.kodetesttask.models.RecipeModel
import com.kodetesttask.util.Urls.URL_API_RECIPES
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface RecipesApi {
    @GET("recipes")
    fun getRecipesAsync(): Deferred<Response<RecipeModel>>

    @GET("recipes/{uuid}")
    fun getRecipeDetailsAsync(
        @Path("uuid") uuid: String
    ): Deferred<Response<RecipeDetailsModel>>

    companion object {
        fun createRetrofit(): RecipesApi = Retrofit.Builder()
            .baseUrl(URL_API_RECIPES)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
            .create(RecipesApi::class.java)
    }
}